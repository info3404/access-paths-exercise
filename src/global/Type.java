package global;

/**
 * Type Enum Class
 * Lists the supported types by the Database
 */
public enum Type {
    STRING(String.class) {
        @Override
        public int getLen() {
            return DatabaseConstants.MAX_STRING_LENGTH;
        }
    },
    DOUBLE(Double.class) {
        @Override
        public int getLen() {
            return 8;
        }
    },
    INTEGER(Integer.class) {
        @Override
        public int getLen() {
            return 4;
        }
    },
    BOOLEAN(Boolean.class) {
        @Override
        public int getLen() {
            return 1;
        }
    };

    private Class<?> typeClass;

    Type(Class<?> typeClass) {
        this.typeClass = typeClass;
    }

    public abstract int getLen();

    public Class<?> getTypeClass() {
        return typeClass;
    }
}
