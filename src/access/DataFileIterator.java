package access;

import java.util.NoSuchElementException;

import buffer.BufferManager;
import buffer.BufferManager.BufferAccessException;
import disk.DataPage;
import disk.PageId;
import heap.Tuple;
import heap.TupleDesc;

/**
 * Iterator over a DataFile (collection of data pages)
 */
public abstract class DataFileIterator extends AccessIterator {

    private PageId dataPageId;
    private DataPage dataPage;
    // An iterator over the current data page in the file
    private DataPageIterator dataPageIterator;
	protected BufferManager bufferManager;
	protected TupleDesc schema;

    /**
     * Takes the PageId of the first page in the file and opens (and pins) it
     * @param dataPageId page id of the first page
     * @param schema 
     * @throws BufferAccessException 
     */
    public DataFileIterator(PageId dataPageId, BufferManager bm, TupleDesc schema) throws BufferAccessException {
    	this.schema = schema;
    	this.bufferManager = bm;
        this.dataPageId = dataPageId;
        this.dataPage = getDataPage(dataPageId);
        this.dataPageIterator = new DataPageIterator(dataPage, schema);
    }

    /**
     * Returns a pinned data page (used to support schema/indexes)
     * @param pageId page of the page to load
     * @return pinned data page corresponding to pageId
     * @throws BufferAccessException 
     */
    protected abstract DataPage getDataPage(PageId pageId) throws BufferAccessException;

    /**
     * Close the iterator (and unpin any pages pinned by it)
     */
    @Override
    public void close() {
        dataPageIterator = null;
        bufferManager.unpin(dataPageId, false);
    }

    /**
     * Checks where there is another record in the file
     * Requires checking not only current page, but the following page
     * in case there are also records on there
     * @return true if there is another record, false if not
     */
    @Override
    public boolean hasNext() {
        return false;
    }

    /**
     * Returns the next tuple in the file.
     * Make sure that you set the page id of the tuple to the page it was read from.
     * i.e. tuple.setPageId(dataPageId);
     */
    @Override
    public Tuple next() {
        return null;
    }
}
