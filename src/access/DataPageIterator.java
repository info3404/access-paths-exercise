package access;

import java.util.Iterator;

import disk.DataPage;
import heap.Tuple;
import heap.TupleDesc;

/**
 * Provides an iterator for the tuples on a page
 */
public class DataPageIterator implements Iterator<Tuple> {

    private DataPage dataPage;
    private int slotNo;
	private TupleDesc schema;

    /**
     * Takes a data page and initialises the current slot as 0
     * @param dataPage the page to iterate over
     * @param schema 
     */
    public DataPageIterator(DataPage dataPage, TupleDesc schema) {
    	this.schema = schema;
        this.dataPage = dataPage;
        this.slotNo = 0;
    }

    /**
     * Whether there is another record on the page to read
	 * - Look at how the pages are stored (hint: slotNo = slot Number?)
     * @return true if current slot number is still within the count on the record
     */
    @Override
    public boolean hasNext() {
        return false;
    }

    /**
     * Returns the next tuple and increments the slot counter
	 * - Note: Check if there is a next one?
     * @return the next tuple on the page
     */
    @Override
    public Tuple next() {
        return null;
    }


    /**
     * Unsupported remove operation. Ignore this.
     */
    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

}
