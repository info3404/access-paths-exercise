package access;

import buffer.BufferManager;
import buffer.BufferManager.BufferAccessException;
import disk.PageId;
import heap.Tuple;
import heap.TupleDesc;
import sorted.SortedFile.ColumnComparator;

public class TreeIndexFileIterator extends AccessIterator {

	/**
	 * Iterator over all records matching on key value
	 * @param startPage First page of sorted file 
	 * @param bufferManager
	 * @param schema
	 * @param comparator
	 * @param ref
	 * @throws BufferAccessException
	 */
	public TreeIndexFileIterator(PageId startPage, BufferManager bufferManager, TupleDesc schema,
			ColumnComparator comparator, Tuple ref) throws BufferAccessException {

	}

	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Tuple next() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
		
	}
	
}

