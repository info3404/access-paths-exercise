package access;

import java.util.NoSuchElementException;

import buffer.BufferManager;
import buffer.BufferManager.BufferAccessException;
import disk.PageId;
import heap.Tuple;
import heap.TupleDesc;
import sorted.SortedFile.ColumnComparator;

/**
 * Performs early termination based upon the given search key
 * @author bjef8061
 *
 */
public class SortedFileIterator extends AccessIterator {
	private HeapFileIterator heapFileIterator;
	private ColumnComparator comparator;
	private Tuple refTuple;
	private boolean atLeastOneMatch;

	/**
	 * Start iterator at the given page in which the first matching entry should be found
	 * @param startPage First page of sorted file that will contain a match, if one exists
	 * @param bufferManager
	 * @param schema
	 * @param comparator
	 * @param ref
	 * @throws BufferAccessException
	 */
	public SortedFileIterator(PageId startPage, BufferManager bufferManager, TupleDesc schema, ColumnComparator comparator, Tuple ref) throws BufferAccessException {
		
	}

    /**
     * Checks whether there is another record in the file
     * Requires checking not only current page, but the following page
     * in case there are also records on there
     * @return true if there is another record, false if not
     */
    @Override
    public boolean hasNext() {
        return false;
    }

    @Override
    public Tuple next() {
       return null;
    }

	@Override
	public void close() {
	}

}
