package access;

import buffer.BufferManager;
import buffer.BufferManager.BufferAccessException;
import disk.DataPage;
import disk.PageId;
import heap.HeapPage;
import heap.TupleDesc;

/**
 * Iterator to traverse over a HeapFile (collection of unordered records)
 */
public class HeapFileIterator extends DataFileIterator {

    public HeapFileIterator(PageId dataFilePage, BufferManager bufferManager, TupleDesc schema) throws BufferAccessException {
        super(dataFilePage, bufferManager, schema);
    }

    @Override
    protected DataPage getDataPage(PageId pageId) throws BufferAccessException {
        return new HeapPage(bufferManager.getPage(pageId), schema);
    }
}
