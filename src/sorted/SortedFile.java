package sorted;

import java.util.ArrayList;
import java.util.Comparator;

import access.AccessIterator;
import access.SortedFileIterator;
import buffer.BufferManager;
import buffer.BufferManager.BufferAccessException;
import disk.PageId;
import global.Type;
import heap.HeapFile;
import heap.HeapPage;
import heap.Tuple;
import heap.TupleDesc;

/**
 * Sorted File
 * A refinement of a Heap File, in which all records are stored in order 
 * according to a specified search key.
 */
public class SortedFile extends HeapFile {

	/*
	 * Class fields
	 * These class details could be stored in a special header page if we were
	 * doing things properly for a full DBMS.
	 */
	
	/**
	 * Object providing means to compare two tuples according to the search key
	 */
	protected ColumnComparator comparator;

	/**
	 * Maintained list of pageIds, ordered according to the key
	 */
	protected ArrayList<PageId> sortedPageIds = new ArrayList<PageId>();

	/**
     * Creates a new SortedFile sorted on the specified column 
	 * @throws BufferAccessException 
	 * @throws SortedFileException 
     */
    public SortedFile(TupleDesc schema, String relationName, BufferManager bm, String columnName) throws BufferAccessException, SortedFileException {
    	this(schema, relationName, bm, new ColumnComparator(schema, columnName));
    }

	private SortedFile(TupleDesc schema, String name, BufferManager bm, ColumnComparator comparator) throws BufferAccessException, SortedFileException {
    	super(schema, name, bm);
    	this.comparator = comparator;
    	if(comparator.columnType==Type.BOOLEAN) {
    		throw new SortedFileException("Boolean attribute not a valid key for sorting");
    	}
    	sortedPageIds.add(firstPageId);
	}

	/**
	 * Inserts a new record into the heapfile
	 * @param values the list of object values that comprise a single tuple
	 * @throws BufferAccessException 
	 */
    @Override
	public void insertRecord(Object... values) throws BufferAccessException {
		Tuple record = new Tuple(schema, values);
		
		// Linear search for correct page
		PageId nextPageId = firstPageId;		
		PageId currentPageId = new PageId(linearSearch(record, nextPageId).get()); // Deep copy to avoid weird bugs
		
		HeapPage page = new HeapPage(bufferManager.getPage(currentPageId), schema);
		
		// Record belongs in the current page (or just after)
		
		// Find insertion position
		int slotNo = 0;
		int nSlots = page.getRecordCount();	
		if(nSlots == 0) {
			page.insertRecord(record);
		} else {
			Tuple displacedTuple = new Tuple(record.getSchema());
			while(slotNo<nSlots) {
				page.getRecord(slotNo, displacedTuple);
				if(comparator.compare(displacedTuple, record)>0) {
					break;
				}
				++slotNo;
			}

			// State:
			// A: record holds record to insert at slot slotNo
			//    displacedTuple (may) holds record displace from slotNo
			// B: slotNo == nSlots && nSlots < page.getMaxRecordsOnPage(record)
			//    record holds record to insert at end of next page
			// C: slotNo == nSlots && nSlots == page.getMaxRecordsOnPage(record)
			//    record holds record to insert at start of new page
			
			if(slotNo < nSlots) { // State A
				// Propagate to end of slots, from State A to State B or C
				while(true) {
					page.insertRecord(slotNo, record);
					if(++slotNo<nSlots) { // More to replace
						page.getRecord(slotNo, record);
	
						// Swap displaceTuple and record without losing one
						// of the associated data structures.
						Tuple nextDisplaced = record;
						record = displacedTuple;
						displacedTuple = nextDisplaced;
						assert displacedTuple != record; // because my head hurts
						
						// State:
						// slotNo points to slot in page
						// record holds record to insert at slot sloNo
						// displacedTuple (may) holds record displace from slotNo					
						
					} else {
						break;
						// State:
						// slotNo == nSlots
						// displacedTuple holds record to insert at slot slotNo
					}
				}
				record = displacedTuple;
			}
			
			// State:
			// B: slotNo == nSlots && nSlots < page.getMaxRecordsOnPage(record)
			//    record holds record to insert at end of next page
			// C: slotNo == nSlots && nSlots == page.getMaxRecordsOnPage(record)
			//    record holds record to insert at start of new page 

			if(nSlots == page.getMaxRecordsOnPage(record)) { // State C
				// Translates from State C to State B (with empty page)
				// insert into new page
				int i = sortedPageIds.indexOf(currentPageId);
				page = createAfter(currentPageId, page);
				// Note currentPageId updated!!!
				sortedPageIds.add(i+1, currentPageId);
			}
			
			// State C: Just need to append the record
			page.insertRecord(record);
		}
		bufferManager.unpin(currentPageId, true);
	}

	/**
	 * Perform linear search of sorted file
	 * @param record Record to find position of
	 * @param currentPageId Starting page for search
	 * @return page ID of page that should contain 1st match (if it exists)
	 * @throws BufferAccessException
	 */
	private PageId linearSearch(Tuple record, PageId firstPageId) throws BufferAccessException {
		Tuple comparisonTuple = new Tuple(schema);
		PageId currentPageId = firstPageId;
		// Looking just before first page
		PageId nextPageId = firstPageId;
		HeapPage nextPage = new HeapPage(bufferManager.getPage(nextPageId), schema);
		PageId pinnedPageId = nextPageId;
		boolean couldBeInNextPage = true;
		while (couldBeInNextPage) {
			currentPageId = nextPageId;
			nextPageId = nextPage.getNextPageId();				
			couldBeInNextPage = nextPageId.isValid();
			if(couldBeInNextPage) {
				bufferManager.unpin(pinnedPageId, false);
				nextPage = new HeapPage(bufferManager.getPage(nextPageId), schema);
				pinnedPageId = nextPageId;
				nextPage.getRecord(0, comparisonTuple);
				if (nextPage.getRecordCount()>0){
					nextPage.getRecord(0, comparisonTuple);
					couldBeInNextPage = !(comparator.compare(comparisonTuple, record)>0); // first entry of current page > record: go no further
				} else {
					couldBeInNextPage = false;
				};
			}
		}
		bufferManager.unpin(pinnedPageId, true);
		return currentPageId;
	}

	/**
	 * Get iterator over all records matching the given sort key value
	 * @param key
	 * @return
	 * @throws BufferAccessException
	 */
	public AccessIterator iterator(Object key) throws BufferAccessException {
		Tuple ref = new Tuple(schema);
		ref.setColumn(comparator.columnIndex, key);
		return null;
//		PageId startPageId = null; // Student to do
//		return new SortedFileIterator(startPageId, bufferManager, schema, comparator, ref);
	}


	/**
     * Compares Two Tuples based on their column
     * Works out the type using the schema, and uses Java's native compare method for the types
     * Used to sort the index file in memory.
     */
    public static class ColumnComparator implements Comparator<Tuple> {

        public int columnIndex;
        public Type columnType;


        public ColumnComparator(TupleDesc schema, String columnName) {
            columnIndex = schema.getIndexFromName(columnName);
            columnType = schema.getFieldType(columnIndex);
        }

        @Override
        public int compare(Tuple left, Tuple right) {
            switch (columnType) {
                case STRING:
                    return ((String)left.getColumn(columnIndex)).compareTo((String)right.getColumn(columnIndex));
                case INTEGER:
                    return Integer.compare((Integer)left.getColumn(columnIndex), (Integer)right.getColumn(columnIndex));
                case DOUBLE:
                    return Double.compare((Double)left.getColumn(columnIndex), (Double)right.getColumn(columnIndex));
                case BOOLEAN:
                    return Boolean.compare((Boolean)left.getColumn(columnIndex), (Boolean)right.getColumn(columnIndex));
                default:
                    throw new IllegalStateException("Invalid Column Type");
            }
        }
    }
    
    public static class SortedFileException extends Exception {

		public SortedFileException() {
		}

		public SortedFileException(String message) {
			super(message);
		}

		public SortedFileException(Throwable cause) {
			super(cause);
		}

		public SortedFileException(String message, Throwable cause) {
			super(message, cause);
		}

		public SortedFileException(String message, Throwable cause, boolean enableSuppression,
				boolean writableStackTrace) {
			super(message, cause, enableSuppression, writableStackTrace);
		}

	}

	ArrayList<PageId> getPageIdList() {
		return sortedPageIds;
	}

}

