package buffer;

import disk.DiskManager;
import disk.Page;
import disk.PageId;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import buffer.replacement.Replacer;

/**
 * Buffer Manager for the database
 */
public class BufferManager {

	private Replacer replacer;
    private List<BufferFrame> frames;
    private int cacheHits;
	private DiskManager diskManager;
	private int maxFrames;

    public BufferManager(int maxFrames, Replacer replacer, DiskManager diskManager) {
        this.replacer = replacer;
        this.maxFrames = maxFrames;
        frames = new LinkedList<BufferFrame>();
        cacheHits = 0;
        this.diskManager = diskManager;
    }

    /**
     * Obtains a page from the buffer.
     * If the page is not in the buffer, it will fetch it from disk
     * @param pageId the id of the page to fetch
     * @return the requested page
     * @throws BufferAccessException 
     */
    public Page getPage(PageId pageId) throws BufferAccessException {
        BufferFrame found = findFrameByPageId(pageId);
        // If page is in the cache (cache hit)
        if(found != null) {
            replacer.notify(frames, found);
            cacheHits++;
            return found.getPage();
        }
        // Get replacement frame
        BufferFrame frame = getBufferFrame();
        replaceFrameInBuffer(pageId, frame);
        return frame.getPage();
    }

    /**
     * Replaces a given BufferFrame with a new page and page id
     * @param pageId the id of the page to insert into the frame
     * @param current the frame to be changed
     * @throws BufferAccessException 
     */
    protected void replaceFrameInBuffer(PageId pageId, BufferFrame current) throws BufferAccessException {
        if(current.isDirty()) {
            flushPage(current);
        }
        Page replacement = new Page();
        try {
            // Read page in from disk and add it to the buffer frame
        	diskManager.readPage(pageId, replacement);
            current.setPage(pageId, replacement);
        }
        catch (BufferFrame.BufferFrameDirtyException | IOException e) {
        	throw new BufferAccessException(e);
        }
        replacer.notify(frames, current);
    }

    /**
     * Flushes all dirty frames in the buffer manager
     * @return number of frames flushed to disk
     * @throws BufferAccessException 
     */
    public int flushDirty() throws BufferAccessException {
        int flushCount = 0;
        for(BufferFrame frame : frames) {
            if(frame.isDirty()) {
                flushPage(frame);
                flushCount++;
            }
        }
        return flushCount;
    }

    /**
     * Writes the page to disk
     * @param frame containing the page to write to disk
     * @throws BufferAccessException 
     */
    public void flushPage(BufferFrame frame) throws BufferAccessException {
        try {
			diskManager.writePage(frame.getPageId(), frame.getPage());
		} catch (IOException e) {
			throw new BufferAccessException(e);
		}
		frame.unpin();
		frame.setDirty(false);
    }

    /**
     * Creates a new page on disk
     * @return id of the new page
     */
    public PageId getNewPage() {
        return diskManager.allocatePage();
    }

    /**
     * Returns the total number of pages allocated on disk
     */
    public long getTotalDiskPages() {
        return diskManager.getNumPages();
    }

    /**
     * Pins a page to the buffer
     * @param pageId id of the page to pin
     */
    public void pin(PageId pageId) {
        BufferFrame found = findFrameByPageId(pageId);
        if(found != null) {
            found.pin();
        }
    }

    /**
     * Declares a page in buffer to be dirty (due to being updated).
     * @param pageId id of the page
     */
    public void markDirty(PageId pageId) {
        BufferFrame found = findFrameByPageId(pageId);
        if(found != null) {
            found.setDirty(true);
        }
    }

    /**
     * Unpins a page from the buffer.
     * @param pageId id of the page to unpin
     * @param isDirty whether the page was modified
     */
    public void unpin(PageId pageId, boolean isDirty) {
        BufferFrame found = findFrameByPageId(pageId);
        if(found != null) {
            if(isDirty)
                found.setDirty(true);
            found.unpin();
        }
    }
    
    /**
     * Finds the frame containing the pageId in the buffer
     * @param pageId the page id to find in the buffer
     * @return the found BufferFrame, or null
     */
    private BufferFrame findFrameByPageId(PageId pageId) {
        for(BufferFrame frame : frames) {
            if(frame.contains(pageId)) {
                return frame;
            }
        }
        return null;
    }

    /**
     * Gets the next buffer frame to be replaced
     * If we haven't allocated all frames yet, we return a new frame
     * If all frames have been allocated, we choose one according to the replacement policy
     */
    private BufferFrame getBufferFrame() {
        if(frames.size() < maxFrames) {
            BufferFrame frame = new BufferFrame();
            frames.add(frame);
            return frame;
        }
        return replacer.choose(frames);
    }

    /**
     * Returns the number of cache hits (for statistics)
     */
    public int getCacheHits() {
        return cacheHits;
    }
    
    /**
     * Returns the number of pinned pages
     */
    public int getNumberOfPinnedPages() {
        int i = 0;
        for(BufferFrame frame : frames) {
            if(frame.isPinned()) {
                i++;
            }
        }
        return i;
    }
	
	public void printStats() {
		StringBuilder builder = new StringBuilder("** Buffer Manager (" + getCacheHits() + " cache hits):");
		for(BufferFrame frame: frames) {
			builder.append(" " + frame.getPageId());
			if(frame.isDirty())
				builder.append("*");
			if(frame.isPinned())
				builder.append("!");				
		}
		System.out.println(builder.toString());
	}
	
    public class BufferAccessException extends Exception {

		public BufferAccessException() {
		}

		public BufferAccessException(String message) {
			super(message);
		}

		public BufferAccessException(Throwable cause) {
			super(cause);
		}

		public BufferAccessException(String message, Throwable cause) {
			super(message, cause);
		}

		public BufferAccessException(String message, Throwable cause, boolean enableSuppression,
				boolean writableStackTrace) {
			super(message, cause, enableSuppression, writableStackTrace);
		}

	}


}
