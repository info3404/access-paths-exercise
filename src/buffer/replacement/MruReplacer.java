package buffer.replacement;

import buffer.BufferFrame;

import java.util.List;

/**
 * Most Recently Used Replacement Policy
 */
public class MruReplacer implements Replacer {
    @Override
    public String getName() {
        return "MRU";
    }

    @Override
    public BufferFrame choose(List<BufferFrame> pool) throws BufferFrame.AllBufferFramesPinnedException {
    	int i = 0; 
		while(i<pool.size()) {
			BufferFrame frame = pool.get(pool.size() - 1 - i);
    		if(!frame.isPinned())
    			return frame;
    		++i;
    	}
        throw new BufferFrame.AllBufferFramesPinnedException();
    }

    @Override
    public void notify(List<BufferFrame> pool, BufferFrame frame) {
        pool.remove(frame);
        pool.add(frame);
    }
    
	@Override
	public void printStats() {
		System.out.println("MRU frame replacer: No state infomation to display.");
	}
	
}
