package buffer.replacement;

import buffer.BufferFrame;

import java.util.List;

/**
 * Interface for Buffer Replacement Strategy implementation
 */
public interface Replacer {

	/**
	 * Returns a unique string for the replacement policy
	 */
	public String getName();

	/**
	 * Chooses the BufferFrame to replace
	 * @param pool current buffer pool
	 * @return chosen buffer frame for replacement
	 */
	public BufferFrame choose(List<BufferFrame> pool) throws BufferFrame.AllBufferFramesPinnedException;

	/**
	 * Called whenever a frame is accessed.
	 * @param pool current buffer pool
	 * @param frame the frame that was accessed
	 */
	public void notify(List<BufferFrame> pool, BufferFrame frame);

	/**
	 * Print information about the current state of the replacer.
	 * You can leave this as an empty method without it affecting the tests.
	 */
	public void printStats();

}