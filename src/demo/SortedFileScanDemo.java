package demo;
import java.io.File;
import java.io.RandomAccessFile;

import access.AccessIterator;
import buffer.BufferManager;
import buffer.replacement.MruReplacer;
import disk.DiskManager;
import disk.PageId;
import global.DatabaseConstants;
import heap.Tuple;
import heap.TupleDesc;
import sorted.SortedFile;

/**
 * Example Execution using File scan of a sorted file.
 * See the test cases for more examples
 */
public class SortedFileScanDemo {

    public static void main(String[] args) {
    	try {
	        // Create the Schema
	        TupleDesc studentSchema = new TupleDesc()
	                .addString("name").addInteger("age").addDouble("speed").addBoolean("male");
	
	        // Give relation an identifying number for storing its data
	        String relationName = "students";
	
	        // Create a new file
			File dbFile = File.createTempFile(DatabaseConstants.DEFAULT_DB_NAME,".tmp");
			dbFile.deleteOnExit();
			DiskManager diskManager = new DiskManager(DatabaseConstants.DEFAULT_DB_NAME,1, new RandomAccessFile(dbFile,"rw"));
	        BufferManager bm = new BufferManager(1, new MruReplacer(), diskManager);
	        SortedFile students = new SortedFile(studentSchema, relationName, bm, "speed");
	
	        // Inserts some records into the heap file
	        final int numRows = 100;
			for(int i = 0; i < numRows; i++) {
	            students.insertRecord("Michael", i/2, -10.0 + i, true);
	            students.insertRecord("Rachael", i*2, 4000.0 - i, false);
	        }
	
	        // Print out the records
	        AccessIterator studentIterator = students.iterator();
	        PageId page=null;
	        int nPages = 0;
	        int nRecords = 0;
	        while(studentIterator.hasNext()) {
	        	++nRecords;
	            final Tuple student = studentIterator.next();
	            if(page!=student.getPageId()) {
	            	page = student.getPageId();
	            	++nPages;
	            }	
				System.out.println("(" + Integer.toString(nRecords) + ") "+ student);
	        }
	        System.out.println("Iterated over " + nRecords + " records in " + nPages + " pages.");
	        
			bm.flushDirty();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
    }

}
