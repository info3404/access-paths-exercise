package index;

import java.util.ArrayList;
import java.util.List;

import access.AccessIterator;
import access.TreeIndexFileIterator;
import buffer.BufferManager;
import buffer.BufferManager.BufferAccessException;
import disk.PageId;
import global.Type;
import heap.HeapPage;
import heap.Tuple;
import heap.TupleDesc;
import sorted.SortedFile;


/**
 * Tree Index Tree Implementation
 * The tree is similar to a B+-tree. All entries are stored within the leaves (a sorted file) in sorted order according to the indexed
 * column. Additional non-leaf pages record index tuples (entries), with the pages organised as a tree structure starting at a root node.
 *
 * Note: This index type is read-only. It is populated using the SortedFile methods, after which buildTree() must be called to creare the
 * non-leaf nodes. Further changes are prohibited after the tree has been constructed.
 */
public class TreeIndexFile extends SortedFile {

	/**
	 * Starting page for searches
	 * In a full DBMS this data would be captured in the header page for the 
	 * while file. Here we're being lazy and hijacking the SortedFile implementation
	 * (which in turn hijacks HeapFile) and keeping this data in memory.
	 */
	protected PageId rootNodeId;

	private TupleDesc indexSchema;
	private ColumnComparator indexComparator; // Allows comparisons between two indexSchema tuples based on the search key
	private static final int INDEX_ENTRY_KEY_COL = 0;
	private static final int INDEX_ENTRY_PAGEID_COL = 1;
	private static final int INDEX_ENTRY_ISLEAF_COL = 2;


	
	/**
	 * Build an index around a sorted file, retaining the sort key
	 * @param leaves The sorted record data
	 * @param bm Buffer manager with which to allocate the tree pages - this 
	 * is typically the same one used by the sorted file.
	 * @throws BufferAccessException
	 * @throws SortedFileException 
	 */
	public TreeIndexFile(TupleDesc schema, String relationName, BufferManager bm, String columnName) throws BufferAccessException, SortedFileException {
		// The sorted file forms the leaf level of the index
		super(schema, relationName, bm, columnName);
		bufferManager=bm;
		
		// Create new schema for tree index tuples (entries)
		indexSchema = new TupleDesc();
		int keyCol = schema.getIndexFromName(columnName);
		Type keyType = schema.getFieldType(keyCol);
        switch (keyType) {
	        case STRING:
	        	indexSchema.addString(columnName);
	        	break;
	        case INTEGER:
	        	indexSchema.addInteger(columnName);
	        	break;
	        case DOUBLE:
	        	indexSchema.addDouble(columnName);
	        	break;
	        case BOOLEAN:
	        	indexSchema.addBoolean(columnName);
	        	break;
	        default:
	            throw new IllegalStateException("Invalid Column Type");
	    }
        indexSchema.addInteger("_pageid");
        indexSchema.addBoolean("_isleaf");
        assert indexSchema.getIndexFromName(columnName) == INDEX_ENTRY_KEY_COL;
        assert indexSchema.getIndexFromName("_pageid") == INDEX_ENTRY_PAGEID_COL;
        assert indexSchema.getIndexFromName("_isleaf") == INDEX_ENTRY_ISLEAF_COL;
        indexComparator = new ColumnComparator(indexSchema, columnName);
        rootNodeId = null; // Set later with buildTree(); 
	}

	/**
	 * Insert new record.
	 * Prevent changes after buildTree has been called
	 * @param values
	 * @throws BufferAccessException
	 */
	@Override
	public void insertRecord(Object... values) throws BufferAccessException {
		if(rootNodeId!=null) throw new IllegalStateException("Tree already constructed");
		super.insertRecord(values);
	}
	
	/**
	 * Incrementally add levels of index pages until only a single (root) page 
	 * is created.
	 * @return Height of tree (including leaf level)
	 * @throws BufferAccessException
	 */
	public int buildTree() throws BufferAccessException {
		if(rootNodeId!=null) throw new IllegalStateException("Tree already constructed");
			
		// NOTE: This should really write directly into a page in the buffer 
		// pool to keep memory access under control, but we'll cheat and use
		// the program heap.
		List<Tuple> nextLevelEntries = getLeafIndexTuples();

		int height = 0;
		if(nextLevelEntries.size()==0) {
			rootNodeId = new PageId(); // Defaults to invalid page
		} else {
			// Recursively build levels of tree
			do {
				++height;
				nextLevelEntries = makeNonLeafLevel(nextLevelEntries);
			} while(nextLevelEntries.size()>1);
	
			// Should end up with a single entry, for which we only need the pageId
			rootNodeId = new PageId((int) nextLevelEntries.get(0).getColumn(INDEX_ENTRY_PAGEID_COL));
		}
		
		return height;
	}

	/**
	 * @param entries List of index entries to each page of the previous level
	 * @return List of index entries to each of the pages created in the new level
	 * @throws BufferAccessException
	 */
	private List<Tuple> makeNonLeafLevel(List<Tuple> entries) throws BufferAccessException {
		List<Tuple> newPageEntries = new ArrayList<Tuple>();
		PageId lastPageId = null;
		HeapPage page = null;
		for(Tuple entry: entries) {
			if(page==null || !page.insertRecord(entry)) {
				if(page!=null) bufferManager.unpin(lastPageId, true);
				lastPageId = bufferManager.getNewPage();
				page = new HeapPage(bufferManager.getPage(lastPageId), indexSchema);				
				newPageEntries.add(makeIndexEntry(entry.getColumn(INDEX_ENTRY_KEY_COL), lastPageId, Boolean.FALSE));				
				page.insertRecord(entry);
			}
		}
		if(page!=null) bufferManager.unpin(lastPageId, true);
		return newPageEntries;
	}

	/**
	 * Traverse the leaf level and identify the first key value that doesn't 
	 * appear in the preceding page, and store an index entry tuple to the page.
	 * 
	 * The index entry tuples contain the following attributes:
	 * <ol>
	 * <li> The key value
	 * <li> The page ID (as an integer)
	 * <li> (Boolean) Flag to indicate whether the entry points to a record (leaf)
	 * page - always true in this method
	 * </ol>
	 * 
	 * @return List of index entry tuples for all candidate pages (note that 
	 * pages containing only tuples with key values that appear in the previous 
	 * page will not be included)
	 * @throws BufferAccessException
	 */
	private List<Tuple> getLeafIndexTuples() throws BufferAccessException {
		List<Tuple> leafEntries;
		leafEntries = new ArrayList<Tuple>();
		int keyCol = comparator.columnIndex;
		AccessIterator it = super.iterator();
		PageId lastPageId = null;
		Tuple lastNewKeyTuple = null;
		while(it.hasNext()) {
			Tuple tuple = it.next();
			if(lastNewKeyTuple == null || comparator.compare(tuple, lastNewKeyTuple)>0) {
				lastNewKeyTuple = tuple;
				PageId pageId = tuple.getPageId();
				if(lastPageId == null || !pageId.equals(lastPageId)) {
					// First tuple within new page that isn't a duplicate of something in previous page
					Tuple indexTuple = makeIndexEntry(lastNewKeyTuple.getColumn(keyCol), pageId, Boolean.TRUE);
					leafEntries.add(indexTuple);
					lastPageId = pageId;
				}
				
			}
		}
		it.close();
		return leafEntries;
	}

	/**
	 * Make new index entry out of specified parameters
	 * @param key Search key value
	 * @param pageId Page Id for page containing first entry >= key
	 * @param isLeaf Flag to indicate whether referenced page is in leaf level
	 * @return New tuple containing index entry
	 */
	private Tuple makeIndexEntry(Object key, PageId pageId, boolean isLeaf) {
		Tuple indexTuple = new Tuple(indexSchema);
		indexTuple.setColumn(INDEX_ENTRY_KEY_COL, key);
		indexTuple.setColumn(INDEX_ENTRY_PAGEID_COL, pageId.get());
		indexTuple.setColumn(INDEX_ENTRY_ISLEAF_COL, isLeaf);
		return indexTuple;
	}

	/**
	 * Returns an iterator over the tree matching on a search key.
	 * @param searchKey the value to use to traverse the tree
	 * @return an iterator to the leaf pages
	 */
	@Override
	public AccessIterator iterator(Object key) throws BufferAccessException {
		if(rootNodeId==null) throw new IllegalStateException("Tree not constructed - need to call buildTree first");
		Tuple ref = new Tuple(schema);
		ref.setColumn(super.comparator.columnIndex, key);
		
		PageId startPageId = null; // STUDENT TO DO
		return new TreeIndexFileIterator(startPageId, bufferManager, schema, comparator, ref);
	}
	
}
