package heap;

import java.util.Iterator;

import access.DataPageIterator;
import disk.DataPage;
import disk.Page;

/**
 * Represents a page full of records in a heap file
 */
public class HeapPage extends DataPage {

    private TupleDesc schema;

	public HeapPage(Page page, TupleDesc schema) {
        this.data = page.getData();
        this.schema = schema;
    }

	public HeapPage(HeapPage heapPage) {
        this.data = heapPage.getData();
        this.schema = heapPage.schema;
    }
	
    public Iterator<Tuple> iterator() {
        return new DataPageIterator(this, schema);
    }

//  TODO: provide schema inference from page header
}
