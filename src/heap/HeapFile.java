package heap;

import access.AccessIterator;
import access.HeapFileIterator;
import buffer.BufferManager;
import buffer.BufferManager.BufferAccessException;
import disk.HeaderPage;
import disk.PageId;

/**
 * Represents a collection of unordered pages containing tuples. (A collection of HeapPages)
 * - Relies on a schema to interpret the Tuples
 */
public class HeapFile {

	protected TupleDesc schema; // All pages in file use this schema
	protected PageId firstPageId; // Starting page for file
	protected BufferManager bufferManager; // Handles access to pages
	protected String relationName; // ID stored in each page to identify its owning relation

	/**
	 * Initialises the HeapFile with the given schema and using a specified 
	 * buffer manager. 
	 * @param schema
	 * @param bm
	 * @throws BufferAccessException 
	 */
	public HeapFile(TupleDesc schema, String relationName, BufferManager bm) throws BufferAccessException {
		this.schema = schema;
		this.relationName = relationName;
		bufferManager = bm;
		
		firstPageId = HeaderPage.getFileEntry(bufferManager, relationName);
		// If we haven't created the HeapFile for this schema yet, then do so
		if(!firstPageId.isValid()) {
			firstPageId = bufferManager.getNewPage();
			HeaderPage.setFileEntry(bufferManager, relationName, firstPageId);
			HeapPage firstPage = new HeapPage(bufferManager.getPage(firstPageId), schema);
			firstPage.initialise(relationName);
			bufferManager.unpin(firstPageId, true);
		}
	}
	
	/**
	 * Inserts a new record into the heapfile
	 * @param values the list of object values that comprise a single tuple
	 * @throws BufferAccessException 
	 */
	public void insertRecord(Object... values) throws BufferAccessException {
		PageId currentPageId = new PageId(firstPageId.get());
		HeapPage currentPage = new HeapPage(bufferManager.getPage(currentPageId), schema);
		Tuple record = new Tuple(schema, values);
		while(!currentPage.insertRecord(record)) {
			// Failed to insert record, need to move to next page or create it if doesn't exist
			if(currentPage.getNextPageId().isValid()) {
				PageId nextPageId = currentPage.getNextPageId();
				bufferManager.unpin(currentPageId, false);
				currentPageId = nextPageId;
				currentPage = new HeapPage(bufferManager.getPage(currentPageId), schema);
			}
			else {
				// We need to create a new page
				HeapPage newPage = createAfter(currentPageId, currentPage);
				currentPage = newPage;
			}
		}
		bufferManager.unpin(currentPageId, true);
	}

	/**
	 * Inserts a new page directly after the current one.
	 * Unpins current page and pins newly created page.
	 * @param currentPageId (gets updated to newly created page's)
	 * @param currentPage
	 * @return Newly create page
	 * @throws BufferAccessException
	 */
	protected HeapPage createAfter(PageId currentPageId, HeapPage currentPage)
			throws BufferAccessException {
		PageId newPageId = bufferManager.getNewPage();
		PageId nextPageId = currentPage.getNextPageId();
		
		currentPage.setNextPageId(newPageId);
		bufferManager.unpin(currentPageId, true);
		
		// Update PreviousPageId of subsequent page
		if(nextPageId.isValid()) {
			HeapPage nextPage = new HeapPage(bufferManager.getPage(nextPageId), schema);
			nextPage.setPreviousPageId(newPageId);
			bufferManager.unpin(nextPageId, true);
		}		
		
		HeapPage newPage = new HeapPage(bufferManager.getPage(newPageId), schema);
		newPage.initialise(relationName);
		newPage.setPreviousPageId(currentPageId);
		newPage.setNextPageId(nextPageId);
		
		currentPageId.set(newPageId.get());
		
		return newPage;
	}

	public AccessIterator iterator() throws BufferAccessException {
		return new HeapFileIterator(firstPageId, bufferManager, schema);
	}

}
