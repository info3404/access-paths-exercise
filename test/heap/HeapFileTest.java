package heap;

import access.AccessIterator;
import buffer.BufferManager;
import buffer.replacement.MruReplacer;
import disk.DiskManager;
import disk.PageId;
import global.DatabaseConstants;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import static org.junit.Assert.*;

import java.io.File;
import java.io.RandomAccessFile;

public class HeapFileTest {

	private static final int NUMSTUDENTS = 1000;

	// Comment this out for debugging
    @Rule
    public Timeout globalTimeout = new Timeout(1000);
    
    private HeapFile student;

	private BufferManager bm;

    @Before
    public void setUp() throws Exception {
		File dbFile = File.createTempFile(DatabaseConstants.DEFAULT_DB_NAME,".tmp");
		dbFile.deleteOnExit();
		DiskManager diskManager = new DiskManager(DatabaseConstants.DEFAULT_DB_NAME,1, new RandomAccessFile(dbFile,"rw"));
		bm = new BufferManager(1, new MruReplacer(), diskManager); // Single frame to force replacement for every page
		
        // Create Test Schema
        TupleDesc studentSchema = new TupleDesc();
        studentSchema.addString("name").addInteger("age").addDouble("speed").addBoolean("male");
        student = new HeapFile(studentSchema, "students", bm);
        
        for(int i=0; i < NUMSTUDENTS; i++) {
            student.insertRecord("Michael", i/2, 200.0 - i, true);
        }
    }

    @Test
    public void testInsertRecord() throws Exception {
        int i = 0;
        AccessIterator studentIterator = student.iterator();
        while(studentIterator.hasNext()) {
            Tuple studentTuple = studentIterator.next();
            assertTrue(studentTuple.rowEquals("Michael", i/2, 200.0 - i, true));
            i++;
        }
        assertEquals("Retrieved all records", NUMSTUDENTS, i);
    }

    @Test
    public void testNoUnpinnedPages() throws Exception {
        int i = 0;
        AccessIterator studentIterator = student.iterator();
        while(studentIterator.hasNext()) {
            studentIterator.next();
            i++;
        }
        assertEquals("Retrieved all records", NUMSTUDENTS, i);
        studentIterator.close();
        assertEquals(0, bm.getNumberOfPinnedPages());
        PageId cheatTest = bm.getNewPage();
        bm.getPage(cheatTest);
        assertEquals(1, bm.getNumberOfPinnedPages());
    }

    @Test
    public void testTupleReturnWithPageIdSet() throws Exception {
        int i = 0;
        AccessIterator studentIterator = student.iterator();
        while(studentIterator.hasNext()) {
            assertTrue("Record " + i++ + " should have valid pageID", studentIterator.next().getPageId().isValid());
        }
        assertEquals("Retrieved all records", NUMSTUDENTS, i);
    }
}