package access;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.RandomAccessFile;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import buffer.BufferManager;
import buffer.BufferManager.BufferAccessException;
import buffer.replacement.MruReplacer;
import disk.DiskManager;
import disk.PageId;
import global.DatabaseConstants;
import heap.Tuple;
import heap.TupleDesc;
import index.TreeIndexFile;

/**
 * Tests of basic functionality for SortedFile.
 * Note that these will not work without a complete implementation of DataFileIterator
 */
public class TreeIndexFileIteratorTest {

	// Comment this out for debugging
	@Rule
	public Timeout globalTimeout = new Timeout(2000);

	private BufferManager bm;
	private DiskManager diskManager;
	private TupleDesc studentSchema;

	@Before
	public void setUp() throws Exception {
		File dbFile = File.createTempFile(DatabaseConstants.DEFAULT_DB_NAME,".tmp");
		dbFile.deleteOnExit();
		diskManager = new DiskManager(DatabaseConstants.DEFAULT_DB_NAME,1, new RandomAccessFile(dbFile,"rw"));
		bm = new BufferManager(1, new MruReplacer(), diskManager); // Single frame to force replacement for every page
		studentSchema = new TupleDesc();
		studentSchema.addString("name").addInteger("age").addDouble("speed").addBoolean("male");
	}

    @Test
    public void testIteratorMatches() throws Exception {
    	String searchKey = "age";
    	int nValues = 50;
    	int recordsPerValue = 20;
    	int nRecords = nValues * recordsPerValue;
    	TreeIndexFile students = new TreeIndexFile(studentSchema, "students", bm, searchKey);
        for(int i = 0; i < nRecords; i++) {
            students.insertRecord("Michael", i % nValues, 1000.0, true);
        }
        
        students.buildTree();

        int keyCol = studentSchema.getIndexFromName(searchKey);
        // Try a reasonable spread of values
        for(int key = 0; key<nValues; key+=nValues/4) {
            AccessIterator studentIterator = students.iterator(key);
            int i = 0;
            while(studentIterator.hasNext()) {
                Tuple studentTuple = studentIterator.next();
                assertEquals(key, studentTuple.getColumn(keyCol));
                i++;
            }
            studentIterator.close();
            assertEquals("Check number of records matching record for key " + key, recordsPerValue, i);
        }
    }


    @Test
    public void testEmptyRelation() throws Exception {
    	TreeIndexFile students = new TreeIndexFile(studentSchema, "students", bm, "name");
    	students.buildTree();
        AccessIterator studentIterator = students.iterator("Michael");
        assertFalse("Empty relation should have no matches", studentIterator.hasNext());
    }

    @Test
    public void testNoMatch() throws Exception {
    	TreeIndexFile students = new TreeIndexFile(studentSchema, "students", bm, "name");
		students.insertRecord("Michael", 0, 200.0, true);
		students.insertRecord("Mitchel", 0, 200.0, true);
		students.buildTree();
        AccessIterator studentIterator = students.iterator("Mervin");
        assertFalse("Iterator on file with no matches should get no results", studentIterator.hasNext());
    }

    @Test
    public void testSingleMatch() throws Exception {
    	TreeIndexFile students = new TreeIndexFile(studentSchema, "students", bm, "name");
		students.insertRecord("Michael", 0, 200.0, true);
		students.insertRecord("Mitchel", 0, 200.0, true);
		students.buildTree();
        AccessIterator studentIterator = students.iterator("Michael");
        assertTrue("Iterator should find matching record", studentIterator.hasNext());
        Tuple tuple = studentIterator.next();
        assertTrue("Retrieved tuple should match original values", tuple.rowEquals("Michael", 0, 200.0, true)); 
        assertFalse("Shouldn't find any other matches after first", studentIterator.hasNext());
    }
    

	@Test
	public void testEarlyTermination() throws Exception {
		final int nTuples = 1000;
		final int nValues = 20;
		final String searchField = "age";
		TreeIndexFile students = new TreeIndexFile(studentSchema, "students", bm, searchField);

		for(int i = 0; i < nTuples; i++) {
			students.insertRecord("Michael", i % nValues, (double)i, true);
		}
		students.buildTree();
		
		final int age = 10;
		AccessIterator studentIterator = students.iterator(age);
		
		assertTrue("First match found", studentIterator.hasNext());
		int accessCount = diskManager.getPageAccesses();
		int nMatchingPages = 1;
		Tuple studentTuple = studentIterator.next();
		PageId lastPageId = studentTuple.getPageId();
		assertNotNull("Tuples should return non-null pageIds", lastPageId);
		
		while(studentIterator.hasNext()) {
			studentTuple = studentIterator.next();
			PageId pageId =  studentTuple.getPageId();
			assertNotNull("Tuples should return non-null pageIds", pageId);
			if(!pageId.equals(lastPageId)) {
				lastPageId = pageId; 
            	++nMatchingPages;
            }	
		}
		accessCount = diskManager.getPageAccesses() - accessCount;
		assertTrue("At most one extra page should be read before terminating read", accessCount< nMatchingPages+1);
	}

	@Test
	public void testTreeSearch() throws Exception {
    	int RECORDS_PER_PAGE = 35; // Found by inspection
    	int ENTRIES_PER_PAGE = 108; // Found by inspection
    	int targetHeight = 2;
    	int nValues = 50;
    	String searchKey = "age";
    	
    	TreeIndexFile students = new TreeIndexFile(studentSchema, "students", bm, searchKey);    	
    	// A completely filled index of height h and will have ENTRIES_PER_PAGE^h 
    	// pages in the leaf level, so need to populate more than this to get a 
    	// higher tree. Here I add 10 to push a bit over the limit.
    	int nTuples = 10+(int) (RECORDS_PER_PAGE * Math.pow(ENTRIES_PER_PAGE, targetHeight-1));
    	for(int i = 0; i < nTuples; i++) {
            students.insertRecord("Michael", i / nValues , 1000.0, true);
        }
        students.buildTree();
        
		// Count pages (and refresh buffer in the process)
        int nPages = countPages(students);
		assertEquals("Number of pages is as expected", (int) Math.ceil(1.0*nTuples / RECORDS_PER_PAGE), nPages);

        /*
         *  Check cost of visiting first record of different values
         *  For a balanced tree this cost should be constant
         */
        // Try a reasonable spread of values
        for(int key = 0; key<nValues; key+=nValues/4) {
        	int accessCount = diskManager.getPageAccesses();
            AccessIterator studentIterator = students.iterator(key);
            accessCount = diskManager.getPageAccesses() - accessCount;
        	assertEquals("Check number of pages accessed to find first matching key " + key, targetHeight, accessCount);
            studentIterator.close();
            
        }
	}

	/**
	 * @param students
	 * @return
	 * @throws BufferAccessException
	 */
	private int countPages(TreeIndexFile students) throws BufferAccessException {
		int nPages = 0;
        AccessIterator studentIterator  = students.iterator();
        PageId lastPageId = new PageId();
		while(studentIterator.hasNext()) {
			Tuple studentTuple = studentIterator.next();
			PageId pageId =  studentTuple.getPageId();
			if(!pageId.equals(lastPageId)) {
				lastPageId = pageId; 
            	++nPages;
            }	
		}
		studentIterator.close();
		return nPages;
	}
	
}
