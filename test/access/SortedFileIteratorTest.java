package access;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.RandomAccessFile;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import buffer.BufferManager;
import buffer.replacement.MruReplacer;
import disk.DiskManager;
import disk.PageId;
import global.DatabaseConstants;
import heap.Tuple;
import heap.TupleDesc;
import sorted.SortedFile;

/**
 * Tests of basic functionality for SortedFile.
 * Note that these will not work without a complete implementation of DataFileIterator
 */
public class SortedFileIteratorTest {

	// Comment this out for debugging
	@Rule
	public Timeout globalTimeout = new Timeout(1000);

	private BufferManager bm;
	private DiskManager diskManager;
	private TupleDesc studentSchema;

	@Before
	public void setUp() throws Exception {
		File dbFile = File.createTempFile(DatabaseConstants.DEFAULT_DB_NAME,".tmp");
		dbFile.deleteOnExit();
		diskManager = new DiskManager(DatabaseConstants.DEFAULT_DB_NAME,1, new RandomAccessFile(dbFile,"rw"));
		bm = new BufferManager(1, new MruReplacer(), diskManager); // Single frame to force replacement for every page
		studentSchema = new TupleDesc();
		studentSchema.addString("name").addInteger("age").addDouble("speed").addBoolean("male");
	}

    @Test
    public void testEmptyRelation() throws Exception {
		SortedFile student = new SortedFile(studentSchema, "students", bm, "name");
        AccessIterator studentIterator = student.iterator("Michael");
        assertFalse("Empty relation should have no matches", studentIterator.hasNext());
    }

    @Test
    public void testNoMatch() throws Exception {
		SortedFile student = new SortedFile(studentSchema, "students", bm, "name");
		student.insertRecord("Michael", 0, 200.0, true);
		student.insertRecord("Mitchel", 0, 200.0, true);

        AccessIterator studentIterator = student.iterator("Mervin");
        assertFalse("Iterator on file with no matches should get no results", studentIterator.hasNext());
    }

    @Test
    public void testSingleMatch() throws Exception {
		SortedFile student = new SortedFile(studentSchema, "students", bm, "name");
		student.insertRecord("Michael", 0, 200.0, true);
		student.insertRecord("Mitchel", 0, 200.0, true);

        AccessIterator studentIterator = student.iterator("Michael");
        assertTrue("Iterator should find matching record", studentIterator.hasNext());
        Tuple tuple = studentIterator.next();
        assertTrue("Retrieved tuple should match original values", tuple.rowEquals("Michael", 0, 200.0, true)); 
        assertFalse("Shouldn't find any other matches after first", studentIterator.hasNext());
    }
    
	@Test
	public void testInsertMultiplePagesInOrder() throws Exception {
		final int nTuples = 1000;
		final int nValues = 50;
		final String searchField = "age";
		SortedFile student = new SortedFile(studentSchema, "students", bm, searchField);

		for(int i = 0; i < nTuples; i++) {
			student.insertRecord("Michael", i % nValues, (double)i, true);
		}
		
		int n = 0;
		final int age = 25;
		AccessIterator studentIterator = student.iterator(age);
		int ageCol = studentSchema.getIndexFromName(searchField);
		while(studentIterator.hasNext()) {
			Tuple studentTuple = studentIterator.next();
			assertEquals("Correct age for student record " + n, age, studentTuple.getColumn(ageCol));
			n++;
		}
		assertEquals("Correct number of matches retrieved", nTuples / nValues, n);
	}

	@Test
	public void testEarlyTermination() throws Exception {
		final int nTuples = 1000;
		final int nValues = 20;
		final String searchField = "age";
		SortedFile student = new SortedFile(studentSchema, "students", bm, searchField);

		for(int i = 0; i < nTuples; i++) {
			student.insertRecord("Michael", i % nValues, (double)i, true);
		}
		
		final int age = 10;
		AccessIterator studentIterator = student.iterator(age);
		
		assertTrue("First match found", studentIterator.hasNext());
		int accessCount = diskManager.getPageAccesses();
		int nMatchingPages = 1;
		Tuple studentTuple = studentIterator.next();
		PageId lastPageId = studentTuple.getPageId();
		assertNotNull("Tuples should return non-null pageIds", lastPageId);
		
		while(studentIterator.hasNext()) {
			studentTuple = studentIterator.next();
			PageId pageId =  studentTuple.getPageId();
			assertNotNull("Tuples should return non-null pageIds", pageId);
			if(!pageId.equals(lastPageId)) {
				lastPageId = pageId; 
            	++nMatchingPages;
            }	
		}
		accessCount = diskManager.getPageAccesses() - accessCount;
		assertTrue("At most one extra page should be read before terminating read", accessCount< nMatchingPages+1);
	}

	@Test
	public void testBinarySearch() throws Exception {
		final int nPages = 8; // Same as in readme example
		final int nTuplesPerPage = 35; // Determined experimentally
		final String searchField = "age";
		int searchCol = studentSchema.getIndexFromName(searchField);
		SortedFile student = new SortedFile(studentSchema, "students", bm, searchField);

		// Pack pages with a different value in each page
		for(int p=0; p<nPages; ++p) {
			for(int i = 0; i < nTuplesPerPage; i++) {
				student.insertRecord("Michael", p, 200.0, true);
			}
		}
		
		// Check that everything is arranged as expected (also flushes buffer)
		int pageCount = 0;
		PageId lastPageId = null;
		AccessIterator it = student.iterator(); 
		while(it.hasNext()) {
			Tuple tuple = it.next();
			PageId pageId =  tuple.getPageId();
			if(!pageId.equals(lastPageId)) {
				lastPageId = pageId; 
            	++pageCount;
            }
			assertEquals("search key consistent with page offset", pageCount-1, tuple.getColumn(searchCol));
		}
		it.close();
		assertEquals("Correct number of pages generated", nPages, pageCount);
		
		/* 
		 * A binary search for tuples in the fifth page (p==age==4)
		 * should follow the sequence 
		 * p3->p5->p4
		 * So expect 3 pages to be accessed
		 */
		final int age = 4;
		int accessCount = diskManager.getPageAccesses();
		AccessIterator studentIterator = student.iterator(age);
		accessCount = diskManager.getPageAccesses() - accessCount;		
		assertTrue("First match found", studentIterator.hasNext());
		assertTrue("Number of pages accessed agrees with binary search (should be " + 3 + " or " + 4 + ")", accessCount == 3 || accessCount == 4);
	}
	
	// TODO: Add a test for many multiple matches spanning pages
}
