package buffer.replacement;

import java.util.LinkedList;
import java.util.List;

import buffer.BufferFrame;
import buffer.replacement.Replacer;
import disk.Page;
import disk.PageId;

/**
 * Created by Scott Sidwell on 20/07/15.
 * Updated by Chris Natoli and Bryn Jeffries
 */
public class TestUtils {

	/**
	 * Create a buffer pool with a specified number of frames and
	 * populate each with a page.
	 * @param numFrames
	 * @return
	 */
    public static List<BufferFrame> generateBufferFrameList(int numFrames) {
        List<BufferFrame> pool = new LinkedList<>();
        for(int i = 0; i < numFrames; i++) {
            BufferFrame temp = new BufferFrame();
            try {
                temp.setPage(new PageId(i), new Page());
            } catch (BufferFrame.BufferFrameDirtyException e) {}
            pool.add(temp);
        }
        return pool;
    }

    /**
     * Call the replacer's notify method multiple times on frames of a pool
     * @param replacer Replacer to call
     * @param pool Buffer pool
     * @param timesToNotify Number of times to call notify() on each frame
     */
	public static void notifyMany(Replacer replacer, List<BufferFrame> pool, int... timesToNotify) {
        for(int i = 0; i < timesToNotify.length; i++) {
            int notify = timesToNotify[i];
            for(int j = 0; j < notify; j++) {
                replacer.notify(pool, pool.get(i));
            }
        }
    }

	public static void simulatePageLoad(Replacer replacer, List<BufferFrame> pool, BufferFrame frame, int pageId) {
		frame.setPage(new PageId(pageId), new Page());
		replacer.notify(pool, frame);
	}

}
