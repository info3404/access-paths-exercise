package disk;

import global.DatabaseConstants;
import global.TestUtils;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class HeaderPageTest {

    private static final PageId INVALID_PAGE = new PageId(DatabaseConstants.INVALID_PAGE_ID);

    private HeaderPage page;
    private PageId pageId;

    @Before
    public void setup() {
        page = new HeaderPage();
        pageId = new PageId(DatabaseConstants.INVALID_PAGE_ID);
    }

    @Test
    public void testInitialise() throws Exception {
        page.initialise();
        assertEquals(INVALID_PAGE, page.getNextPage());
        PageId temp = new PageId(DatabaseConstants.INVALID_PAGE_ID);
        for(int i = 0; i < page.getNumPointers(); i++) {
            String entry = page.getFileEntry(0, temp);
            assertEquals("", entry);
            assertEquals(INVALID_PAGE, temp);
        }
    }

    @Test
    public void testSetNextPage() throws Exception {
        page.initialise();
        assertEquals(INVALID_PAGE, page.getNextPage());
        PageId newPageId = new PageId(1);
        page.setNextPage(newPageId);
        assertEquals(newPageId, page.getNextPage());
    }

    @Test(expected=RuntimeException.class)
    public void testNullSetNextPage() {
        page.initialise();
        page.setNextPage(null);
    }

    @Test
    public void testNumPointers() throws Exception {
        page.initialise();
        int numPointers = page.getNumPointers();
        page.setNumPointers(100);
        assertEquals(100, page.getNumPointers());
        page.setNumPointers(-1);
        assertEquals(-1, page.getNumPointers());
    }

//    public void writeFileEntry(int record, int newPageId) throws Exception {
//        String max = TestUtils.generateAsciiString(newPageId, DatabaseConstants.MAX_RELATION_NAME_LENGTH);
//        page.setFileEntry(pageId, max, record);
//        PageId temp = new PageId(DatabaseConstants.INVALID_PAGE_ID);
//        assertEquals(max, page.getFileEntry(record, temp));
//        assertEquals(pageId, temp);
//    }

    @Test
    public void testMultipleFileEntry() throws Exception {
        page.initialise();
        int top = Integer.MAX_VALUE;
        pageId.set(top);
        // Arbitrarily writing values to every record in the page
        for(int record = 0; record < page.getNumPointers(); record++) {
            String max = TestUtils.generateAsciiString(top - record, DatabaseConstants.MAX_RELATION_NAME_LENGTH);
            pageId.set(top-record);
            page.setFileEntry(pageId, max, record);
        }
        // We then read through it again and hope that our values are intact
        for(int record = 0; record < page.getNumPointers(); record++) {
            String max = TestUtils.generateAsciiString(top - record, DatabaseConstants.MAX_RELATION_NAME_LENGTH);
            String result = page.getFileEntry(record, pageId);
            assertEquals(max, result);
            assertEquals(pageId, new PageId(top - record));
        }
    }


}