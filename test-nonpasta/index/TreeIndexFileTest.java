package index;

import static org.junit.Assert.*;

import java.io.File;
import java.io.RandomAccessFile;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import buffer.BufferManager;
import buffer.replacement.MruReplacer;
import disk.DiskManager;
import global.DatabaseConstants;
import heap.TupleDesc;

public class TreeIndexFileTest {

	// Comment this out for debugging
//    @Rule
//    public Timeout globalTimeout = new Timeout(1000);
    
	private BufferManager bm;
	private TupleDesc studentSchema;

    @Before
    public void setUp() throws Exception {
		File dbFile = File.createTempFile(DatabaseConstants.DEFAULT_DB_NAME,".tmp");
		dbFile.deleteOnExit();
		DiskManager diskManager = new DiskManager(DatabaseConstants.DEFAULT_DB_NAME,1, new RandomAccessFile(dbFile,"rw"));
		bm = new BufferManager(1, new MruReplacer(), diskManager); // Single frame to force replacement for every page
		
        // Create Test Schema
        studentSchema = new TupleDesc();
        studentSchema.addString("name").addInteger("age").addDouble("speed").addBoolean("male");
        
    }

    @Test
    public void testBuildTreeEmpty() throws Exception {
    	TreeIndexFile students = new TreeIndexFile(studentSchema, "students", bm, "age");
        int height = students.buildTree();
        assertEquals("Empty tree has height 0",0,height);
    }

    @Test
    public void testBuildTreeSmall() throws Exception {
    	TreeIndexFile students = new TreeIndexFile(studentSchema, "students", bm, "age");
        for(int i = 0; i < 3; i++) {
            students.insertRecord("Michael", i/2, 1000.0 - i, true);
        }
        int height = students.buildTree();
        assertEquals("Tree with very small number of records has height 1",1,height);
    }

    // Note: this is likely to take a few seconds to run
    @Test
    public void testBuildTreeBig() throws Exception {
    	int RECORDS_PER_PAGE = 35;
    	int ENTRIES_PER_PAGE = 108; // Found by inspection
    	TreeIndexFile students = new TreeIndexFile(studentSchema, "students", bm, "age");
    	// WARNING: Grows exponentially - anything more than 2 will take a long time
    	// (main delay is caused by the use of linearSearch in SortedFile)
    	int targetHeight = 2; 
    	
    	// A completely filled index of height h and will have ENTRIES_PER_PAGE^h 
    	// pages in the leaf level, so need to populate more than this to get a 
    	// higher tree. Here I add 10 to push a bit over the limit.
    	int nTuples = 10+(int) (RECORDS_PER_PAGE * Math.pow(ENTRIES_PER_PAGE, targetHeight-1));
    	for(int i = 0; i < nTuples; i++) {
            students.insertRecord("Michael", i, 1000.0, true);
        }
        int height = students.buildTree();
        assertEquals("Height is as expected", targetHeight,height);
    }

    @Test
    public void testNoInsertAfterBuildTree() throws Exception {
    	TreeIndexFile students = new TreeIndexFile(studentSchema, "students", bm, "age");
        for(int i = 0; i < 1000; i++) {
            students.insertRecord("Michael", i/2, 1000.0 - i, true);
        }
        students.buildTree();
        boolean exceptionThrown = false;
        try {
        	students.insertRecord("Michael", 42, 1000, true);
        } catch (IllegalStateException e) {
        	exceptionThrown = true;
        }
        assertTrue("Insertion after buildTree() should throw an exception", exceptionThrown);
    }

    @Test
    public void testCantSearchUntilBuildTree() throws Exception {
    	TreeIndexFile students = new TreeIndexFile(studentSchema, "students", bm, "age");
        for(int i = 0; i < 1000; i++) {
            students.insertRecord("Michael", i/2, 1000.0 - i, true);
        }
        boolean exceptionThrown = false;
        try {
        	students.iterator(50);
        } catch (IllegalStateException e) {
        	exceptionThrown = true;
        }
        assertTrue("iterator(Object) before buildTree() should throw an exception", exceptionThrown);
    }

}