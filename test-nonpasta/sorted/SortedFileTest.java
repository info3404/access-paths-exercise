package sorted;

import static org.junit.Assert.*;

import java.io.File;
import java.io.RandomAccessFile;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import access.AccessIterator;
import buffer.BufferManager;
import buffer.BufferManager.BufferAccessException;
import buffer.replacement.MruReplacer;
import disk.DiskManager;
import disk.PageId;
import global.DatabaseConstants;
import heap.Tuple;
import heap.TupleDesc;
import sorted.SortedFile.SortedFileException;

/**
 * Tests of basic functionality for SortedFile.
 * Note that these will not work without a complete implementation of DataFileIterator
 */
public class SortedFileTest {

	// Comment this out for debugging
	@Rule
	public Timeout globalTimeout = new Timeout(1000);

	BufferManager bm;
	private DiskManager diskManager;
	private TupleDesc studentSchema;

	@Before
	public void setUp() throws Exception {
		File dbFile = File.createTempFile(DatabaseConstants.DEFAULT_DB_NAME,".tmp");
		dbFile.deleteOnExit();
		diskManager = new DiskManager(DatabaseConstants.DEFAULT_DB_NAME,1, new RandomAccessFile(dbFile,"rw"));
		bm = new BufferManager(1, new MruReplacer(), diskManager); // Single frame to force replacement for every page
		studentSchema = new TupleDesc();
		studentSchema.addString("name").addInteger("age").addDouble("speed").addBoolean("male");
	}


	@Test
	public void testInsertSingleRecord() throws Exception {
		SortedFile student = new SortedFile(studentSchema, "students", bm, "name");

		student.insertRecord("Michael", 0, 200.0, true);

		// Check it's there
		AccessIterator studentIterator = student.iterator();
		assertTrue(studentIterator.hasNext());
		Tuple studentTuple = studentIterator.next();
		assertEquals(studentSchema, studentTuple.getSchema());
		assertTrue(studentTuple.rowEquals("Michael", 0, 200.0, true));

		assertFalse("Only one record should be in the file", studentIterator.hasNext());
	}

	@Test
	public void testInsertMultiplePagesInOrder() throws Exception {
		final int nTuples = 1000;
		SortedFile student = new SortedFile(studentSchema, "students", bm, "speed");

		for(int i = 0; i < nTuples; i++) {
			student.insertRecord("Michael", i/2, (double)i, true);
		}
		
		int n = 0;
		AccessIterator studentIterator = student.iterator();
		int speedCol = studentSchema.getIndexFromName("speed");
		while(studentIterator.hasNext()) {
			Tuple studentTuple = studentIterator.next();
			assertEquals("Student record " + n, (double)n, studentTuple.getColumn(speedCol));
			assertTrue("Student record " + n, studentTuple.rowEquals("Michael", n/2, (double)n, true));
			n++;
		}
		assertEquals(nTuples, n);
	}

	@Test
	public void testPageIdList() throws Exception {
		SortedFile student = new SortedFile(studentSchema, "students", bm, "age");
		assertEquals("Single page for empty relation",1, student.getPageIdList().size());
		
		final int nTuples = 100;
		// Insert out of order to force unordered page Ids
		for(int i = 0; i < nTuples; i++) {
			student.insertRecord("Michael", i % 20, 500.0, true);
		}
		
		ArrayList<PageId> pageIds = new ArrayList<PageId>();
		int n = 0;
		PageId pageId=null;
		AccessIterator studentIterator = student.iterator();
		int ageCol = studentSchema.getIndexFromName("age");
		Integer firstVal = null;
		while(studentIterator.hasNext()) {
			Tuple studentTuple = studentIterator.next();
            if(pageId!=studentTuple.getPageId()) {
            	Integer newFirstVal = (Integer) studentTuple.getColumn(ageCol);
    			assertTrue("Pages in correct order " + n, (firstVal == null && newFirstVal != null) || newFirstVal >= firstVal);
    			firstVal = newFirstVal;
            	pageId = studentTuple.getPageId();
            	pageIds.add(pageId);
            }	
			n++;
		}
		assertEquals("All tuples returned", nTuples, n);
		
		assertEquals("Page Ids match", pageIds, student.getPageIdList());

	}
	
	@Test
	public void testInsertReverseOrderSmall() throws Exception {
		final int nTuples = 3;
		testInsertReverseN(nTuples);
	}

	/**
	 * @param nTuples
	 * @throws BufferAccessException
	 * @throws SortedFileException
	 */
	private void testInsertReverseN(final int nTuples) throws BufferAccessException, SortedFileException {
		SortedFile student = new SortedFile(studentSchema, "students", bm, "speed");

		double v = 0;
		for(int i = 0; i < nTuples; i++) {
			v = 500.0 - i;
			student.insertRecord("Michael", i/2, v, true);
		}

		int n = 0;
		AccessIterator studentIterator = student.iterator();
		int speedCol = studentSchema.getIndexFromName("speed");
		while(studentIterator.hasNext()) {
			Tuple studentTuple = studentIterator.next();
			assertEquals("Student record " + n, v + n, studentTuple.getColumn(speedCol));
			n++;
		}
		assertEquals(nTuples, n);
	}

	@Test
	public void testInsertReverseOrderLarge() throws Exception {
		final int nTuples = 1000;
		testInsertReverseN(nTuples);
	}


	@Test
	public void testNoUnpinnedPages() throws Exception {
		final int nTuples = 1000;
		SortedFile student = new SortedFile(studentSchema, "students", bm, "speed");

		for(int i = 0; i < nTuples; i++) {
			student.insertRecord("Michael", i/2, (double)i, true);
		}

		int n = 0;
		AccessIterator studentIterator = student.iterator();
		while(studentIterator.hasNext()) {
			studentIterator.next();
			n++;
		}
		assertEquals("Retrieved all tuples", nTuples, n);
		studentIterator.close();
		assertEquals(0, bm.getNumberOfPinnedPages());
		PageId cheatTest = bm.getNewPage();
		bm.getPage(cheatTest);
		assertEquals(1, bm.getNumberOfPinnedPages());
	}

	@Test
	public void testTupleReturnWithPageIdSet() throws Exception {
		final int nTuples = 1000;
		SortedFile student = new SortedFile(studentSchema, "students", bm, "speed");

		for(int i = 0; i < nTuples; i++) {
			student.insertRecord("Michael", i/2, (double)i, true);
		}

		AccessIterator studentIterator = student.iterator();
		while(studentIterator.hasNext()) {
			assertTrue(studentIterator.next().getPageId().isValid());
		}
	}

	@Test
	public void testBooleanFilesNotAllowed() throws Exception {
		boolean exceptionThrown = false;
		try {
			@SuppressWarnings("unused")
			SortedFile student = new SortedFile(studentSchema, "students", bm, "male");
		} catch (SortedFileException e) {
			exceptionThrown = true;
		}
		assertTrue("Boolean type keys should cause exception to be throws", exceptionThrown);  
	}
}
