# README #

This is the skeleton code you will use for the Week 3 PASTA homework tasks.

Inside you will find a number of Java files.

This week, there are three different but related tasks:

1. Easy: Heap File Scans
 - Pass all the tests in `HeapFileTest`
 - Implement the `DataPageIterator` and `DataFileIterator` classes in the `access` package
 
2. Medium: Sorted File Scans
 - You first need to implement the Easy task
 - Pass all the tests in `SortedFileIteratorTest`
 - You'll need to implement `SortedFile.iterator(Object key)` and the methods in `SortedFileIterator`, to use binary search to find the first match and terminate scan after no further matches are found.

3. Hard: Tree Index Scans
 - You first need to implement the Easy task
 - Pass all the tests in `TreeFileIteratorTest`
 - You'll need to implement the methods in `access.TreeIndexIterator` and `index.TreeIndexFile` to traverse the tree structure to find the first matching tuple, and terminate the scan after no more matches are found

To submit, zip up the src folder and upload it to PASTA

## Suggestions ##

Look at the `HeapFile.pdf` slides provided with this source to get some information on how Heap Files are traversed using `DataPageIterator` and `DataFileIterator`.

Simple examples of using some of the access methods are included in the `demo` package in the `src` folder.

Some unit tests that are NOT tested by PASTA are included for sanity checking in the `test-nonpasta` folder. These may provide useful examples of how some of the classes are used.

Note that this time the page ID number is wrapped in a PageID class to provide useful methods, and the methods from before have been updated accordingly.

As with normal buffer managers, the BufferManager class now pins newly accessed pages, and so they must be explicitly unpinned to be released. This is handled for you in the existing code. 

Getting your head around the Tuple and TupleDesc class will be important for being able to read tuples from disk. (Hint: getRecord).
There are plenty of examples scattered throughout the scaffold code where these methods have been used. It might be helpful to use "Find Usages" on methods that you're unsure how to use.

### Binary Search ###

You will probably need to modify `SortedFile` to use binary search when creating a `SortedFileIterator` within the `iterator(Object key)` method.

There are actually a variety of forms of binary search, with many special cases. For consistency, we'll go with the algorithm given in [Wikipedia](https://en.wikipedia.org/wiki/Binary_search_algorithm#Procedure):

    1. Set L to 0 and R to n - 1.
    2. If L > R, the search terminates as unsuccessful.
    3. Set m (the position of the middle element) to the floor (the largest previous integer) of (L + R)/2.
    4. If Am < T, set L to m + 1 and go to step 2.
    5. If Am > T, set R to m - 1 and go to step 2.
    6. Now Am = T, the search is done; return m.

So, for example, if you had 8 pages {A, B, C, D, E, F, G, H}, the first page to check would be D ( m = floor((0+7)/2) = 3 ). If the first record's value was greater than the search value T we'd go backwards to page B ( m = floor((0+(3-1))/2) = 1). If B's last record's value was less than T we'd then go forwards to page C ( m = floor(((1+1)+2)/2) = 2). If we couldn't find T in this page we'd have to terminate the search as unsuccessful.
Similarly searching for something that's in page E would involve checks of D ( m = floor((0+7)/2) = 3: too low) -> F ( m = floor(((3+1)+7)/2) = 5: too high) -> E ( m = floor((4+(5-1))/2) = 4: right page).

For binary search it is useful to be able to access the pageIds in sequence. Support is provided for you with the `SortedFile.getPageIdList()` method.

A linear search method is already provided for you in `SortedFile.linearSearch()`, used for insertion, and this will give you some idea how you would start with binary search.

### Tree Search ###
For those doing hard: Have a read of `index/TreeIndexFile.java` to first understand the structure of the tree.

## Thoughts to keep in mind ##

* What type of page architecture is used to store records? How does it handle variable-length records? Check `DataPage.write()`
* Why do we use indexes vs just heap files?
* What are the benefits of using a sorted file over a normal heap file?
* The data on disk is just bytes, it's only with the schema we can interpret it: why?
* What benefits do tree indexes provide over heap files. When wouldn't you use them?